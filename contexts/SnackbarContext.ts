import { Severity } from './../interfaces/alert.severity.interface';
import { createContext } from 'react';

interface ISharedBarContextProps {
  openBar(message: string, severity: Severity): void,
  closeBar(): void,
  isOpen: boolean,
  message: string,
  severity: Severity
}

export const SharedBarContext = createContext({} as ISharedBarContextProps);

export default SharedBarContext;