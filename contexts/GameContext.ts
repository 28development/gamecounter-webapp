import IGame from './../interfaces/game.interface';
import { createContext } from 'react';

const GameContext = createContext<IGame>({} as IGame);

export default GameContext;