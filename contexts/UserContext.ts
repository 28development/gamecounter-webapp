import { IUser } from './../interfaces/user.interface';
import { createContext } from 'react';

const UserContext = createContext<IUser>({} as IUser);

export default UserContext;