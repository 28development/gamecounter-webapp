import { Severity } from './../../interfaces/alert.severity.interface';
import axios from 'axios';
import IGame from '../../interfaces/game.interface';

export const baseRoute = `${process.env.API_URL}${process.env.API_PORT}`;

// user routes
export const meRoute = `${baseRoute}/api/users/me`;
export const loginRoute = `${baseRoute}/api/users/login`;

export const toggleGameFavorites = async (userId: string, game: IGame, favorites: Array<string>, token: string) => {
  return favorites.includes(game._id) ?
    await detachGameFromFavorites(userId, game, token) :
    await addGameToFavorites(userId, game, token);
}

export const addGameToFavorites = async (userId: string, game: IGame, token: string) => {
  const url: string = `${baseRoute}/api/users/${userId}/favorites/push`;
  const headers = { Authorization: `Bearer ${token}` };
  const { data } = await axios.patch(url, { id: game._id }, { headers });
  const message = `Game ${game.title} has been added to the favorites`;
  const severity: Severity = 'success';

  return { data, message, severity };
}

export const detachGameFromFavorites = async (userId: string, game: IGame, token: string) => {
  const url: string = `${baseRoute}/api/users/${userId}/favorites/remove`;
  const headers = { Authorization: `Bearer ${token}` };
  const { data } = await axios.patch(url, { id: game._id }, { headers });
  const message = `Game ${game.title} has been removed from the favorites.`;
  const severity: Severity = 'warning';

  return { data, message, severity };
}

// game routes
export const randomGameRoute = `${baseRoute}/api/game/random`;
export const getGamesRoute = `${baseRoute}/api/game`;

export const getGameRoute = (title: string | string [] | undefined): string => {
  return `${baseRoute}/api/game/${title}`;
}

// vibrant proxy route, since vibrant has still a cors bug ...
export const setProxyImgUrl = (url: string | undefined): string => {
  return `https://cors-anywhere.herokuapp.com/${url}`;
}