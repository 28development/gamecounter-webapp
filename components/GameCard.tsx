import { Avatar, CardHeader, Chip, Divider, Icon, IconButton } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import FaceIcon from '@material-ui/icons/Face';
import LaunchIcon from '@material-ui/icons/Launch';
import ShareIcon from '@material-ui/icons/Share';
import Shop from '@material-ui/icons/Shop';
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import React, { FunctionComponent, useContext, useEffect, useState } from 'react';
import { Cookies } from 'react-cookie';
import hearticonstyle from '../components/css/hearticonstyle.module.scss';
import SharedBarContext from '../contexts/SnackbarContext';
import UserContext from '../contexts/UserContext';
import { Severity } from '../interfaces/alert.severity.interface';
import IGame from '../interfaces/game.interface';
import { toggleGameFavorites } from '../lib/api/routes';
import Link from '../src/Link';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  cardHeaderRoot: {
    overflow: "hidden"
  },
  cardHeaderContent: {
    overflow: "hidden"
  },
  cardHeaderAction: {
    marginTop: '0',
    marginRight: '0'
  },
  divider: {
    margin: `${theme.spacing(3)}px 0`
  },
  avatar: {
    display: "inline-block",
    border: "2px solid white",
    "&:not(:first-of-type)": {
      marginLeft: -theme.spacing()
    }
  }
}));

type GameCardProps = {
  game: IGame
}

const Alert = (props: AlertProps) => {
  return (
    <MuiAlert elevation={6} variant="filled" {...props} />
  )
}

const GameCard: FunctionComponent<GameCardProps> = props => {
  const cookies = new Cookies();
  const classes = useStyles();
  const [favMsg, setFavMsg] = useState('');
  const [open, setOpen] = useState(false);
  const [favorites, setFavorites] = useState<string[]>([]);
  const [favBurst, setFavBurst] = useState(false);
  const [favDetach, setFavDetach] = useState(false);
  const [activeGameId, setActiveGameId] = useState('');
  const user = useContext(UserContext);
  const { openBar } = useContext(SharedBarContext);

  useEffect(() => {
    setFavorites(() => [...user.favorites])
  }, []);

  const addToFavorites = async (game: IGame) => {
    try {
      const token = cookies.get('token');
      setActiveGameId(game._id);
      !isFavorite(game._id) ? setFavBurst(true) : setFavDetach(true);
      const { data, message, severity } = await toggleGameFavorites(user._id, game, favorites, token);
      onAddFavSuccess(data, message, severity);
    } catch (e) {
      onAddFavErr(e);
    }
  }

  const faces = [
    "http://i.pravatar.cc/300?img=1",
    "http://i.pravatar.cc/300?img=2",
    "http://i.pravatar.cc/300?img=3",
    "http://i.pravatar.cc/300?img=4"
  ];

  const expandCard = () => {

  }

  const onAddFavSuccess = (data: any, message: string, severity: Severity) => {
    setFavorites(favorites => [favorites, ...data]);
    setFavBurst(false);
    openBar(message, severity);
  }

  const onAddFavErr = (e: any) => {
    console.log(Object.keys(e), e.message);
    openBar("An error occured.", "error");
    setFavBurst(false);
  }

  const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') return;
    setOpen(false);
  };

  const isFavorite = (gameId: string): boolean => {
    if (favorites.includes(gameId)) return true;
    return false;
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        classes={{
          root: classes.cardHeaderRoot,
          content: classes.cardHeaderContent,
          action: classes.cardHeaderAction
        }}
        action={
          <Grid container alignItems="flex-start" justify="flex-end" direction="row">
            <IconButton aria-label="add to favorites" onClick={() => addToFavorites(props.game)}>
              {isFavorite(props.game._id) ? (
                <Icon
                  fontSize="large"
                  className={[
                    hearticonstyle.heart,
                    hearticonstyle.isActive].join(" ")}></Icon>
              ) : (
                  <Icon
                    fontSize="large"
                    className={[
                      hearticonstyle.heart,
                      favBurst && activeGameId == props.game._id ?
                        hearticonstyle.isBursting :
                        ''].join(" ")}></Icon>
                )}
            </IconButton>
          </Grid>
        }
        title={
          <Typography noWrap gutterBottom variant="h6" component="h4">
            {props.game.title}
          </Typography>
        }
      />
      <Link scroll={true} href='/games/[title]' as={`/games/${props.game._id}`}>
        <CardMedia
          className={classes.cardMedia}
          image={props.game.thumbnail}
          title={props.game.title}
        />
      </Link>
      <CardContent className={classes.cardContent}>
        <Chip
          icon={<FaceIcon />}
          label={props.game.genre}
          clickable
          color="primary"
          variant="outlined"
        />
        <Divider className={classes.divider} light />
        {faces.map(face => (
          <Avatar className={classes.avatar} key={face} src={face} />
        ))}
      </CardContent>
      <CardActions disableSpacing>
        <IconButton aria-label="share">
          <ShareIcon />
        </IconButton>
        <Link scroll={true} href='/games/[title]' as={`/games/${props.game._id}`}>
          <IconButton aria-label="Open">
            <LaunchIcon />
          </IconButton>
        </Link>
        <a href={props.game.amazonRefLink} target="_blank">
          <IconButton aria-label="Open">
            <Shop />
          </IconButton>
        </a>
        <Grid container alignItems="flex-start" justify="flex-end" direction="row">
          <IconButton aria-label="Open">
            <ExpandMoreIcon onClick={() => expandCard() } />
          </IconButton>
        </Grid>
      </CardActions>
    </Card>
  );
}

export default GameCard;