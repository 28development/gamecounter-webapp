import { Avatar, Typography, Divider, makeStyles, Grid } from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  headerSpacing: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  }
}));

const avatarSrc = "https://de.gravatar.com/userimage/151688790/61611659f3a1ac6334cc5f3462ee4b43.png?size=200";

const DrawerHeader = () => {
  const classes = useStyles();

  return (
    <>
      <Grid container className={classes.headerSpacing} style={{ overflow: 'hidden' }}>
        <Grid item xs={12} sm={6} md={3}>
          <Avatar src={avatarSrc} />
        </Grid>
        <Divider />
      </Grid>
    </>
  )
}

export default DrawerHeader;