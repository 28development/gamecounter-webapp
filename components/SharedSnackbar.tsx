import { useContext } from "react";
import { Snackbar, IconButton } from "@material-ui/core";
import { Close } from "@material-ui/icons";
import SharedBarContext from "../contexts/SnackbarContext";
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { Severity } from "../interfaces/alert.severity.interface";

const Alert = (props: AlertProps) => {
  return (
    <MuiAlert elevation={6} variant="filled" {...props} />
  )
}

const SharedBar = () => {
  const { isOpen, message, closeBar, severity } = useContext(SharedBarContext);

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      open={isOpen}
      autoHideDuration={4000}
      onClose={closeBar}
    >
      <Alert onClose={closeBar} severity={severity}>{message}</Alert>
    </Snackbar>
  );
};

export default SharedBar;
