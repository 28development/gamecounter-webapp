import { CssBaseline, CircularProgress, createStyles, Theme, StylesProvider, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Head from 'next/head';
import * as React from 'react';
import UserContext from '../contexts/UserContext';
import { Cookies } from 'react-cookie';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { meRoute } from '../lib/api/routes';
import { useRouter } from 'next/router';
import { IUser } from '../interfaces/user.interface';
import TopAppBar from './TopAppBar';
import { SharedBarProvider } from './SharedSnackbarContext';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      margin: 'auto'
    },
    cardGrid: {
      paddingTop: theme.spacing(8),
      paddingBottom: theme.spacing(8),
    }
  }),
);

type Props = {
  title?: string
}

const Layout: React.FunctionComponent<Props> = ({ children, title = 'GameCounter.io' }) => {
  const cookies = new Cookies();
  const classes = useStyles();
  const router = useRouter();
  const [colorGradient, setColorGradient] = useState('linear-gradient(45deg, #1D70B6 50%, #29235C 90%)');
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState<IUser>({} as IUser);

  const getUser = async () => {
    const token = cookies.get('token');
    const headers = { Authorization: `Bearer ${token}` };
    const { data } = await axios.get(meRoute, { headers });
    setUser(data);
    setLoading(false);
  }

  useEffect(() => {
    const token = cookies.get('token');
    if (!token) router.push('/');
    getUser();
  }, []);

  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="stylesheet" type="text/css" href="/nprogress.css" />
      </Head>
      <CssBaseline />
      <main>
        {loading ?
          <div>
            <TopAppBar color={colorGradient} />
            <Container className={classes.cardGrid} maxWidth="md">
              <CircularProgress className={classes.root} />
            </Container>
          </div> :
          <UserContext.Provider value={user}>{children}</UserContext.Provider>
        }
      </main>
    </div>
  )
};

export default Layout;