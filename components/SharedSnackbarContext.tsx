import { Snackbar } from '@material-ui/core';
import { IUser } from './../interfaces/user.interface';
import { createContext, useState } from 'react';
import SharedBarContext from '../contexts/SnackbarContext';
import SharedBar from './SharedSnackbar';
import { Severity } from '../interfaces/alert.severity.interface';

export const SharedBarProvider = (props: { children: any; }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [severity, setSeverity] = useState({} as Severity);

  const openBar = (message: string, severity: Severity) => {
    setSeverity(severity);
    setMessage(message);
    setIsOpen(true);
  };

  const closeBar = () => {
    setMessage("");
    setIsOpen(false);
  };

  const { children } = props;

  return (
    <SharedBarContext.Provider
      value={{
        openBar,
        closeBar,
        isOpen,
        message,
        severity
      }}
    >
      <SharedBar />
      {children}
    </SharedBarContext.Provider>
  );
};