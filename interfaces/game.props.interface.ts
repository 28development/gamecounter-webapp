import IGame from './game.interface';

interface IGameProps {
  id: string,
  userId: string,
  game?: IGame,
  games?: [IGame],
  token: string,
  query: string
}

export default IGameProps;