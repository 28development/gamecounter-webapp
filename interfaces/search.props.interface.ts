import IGame from './game.interface';

interface ISearchProps {
  search: string
}

export default ISearchProps;