interface IGame {
  _id: string;
  isAdmin: Boolean;
  title: string;
  description: string;
  genre: [];
  thumbnail: string;
  image: string;
  homepage: string;
  youtubeTrailer: string;
  amazonRefLink: string,
  subscribers: [];
  release_at: Date;
}

export default IGame;