export interface IUser {
  _id: string;
  isAdmin: Boolean;
  name: string;
  email: string;
  password: string;
  avatar: string;
  tokens?: [];
  favorites: [string];
  created_at: Date;
  updated_at: Date;
}