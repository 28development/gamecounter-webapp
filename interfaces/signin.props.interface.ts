import IGame from './game.interface';

interface IGameProps {
  game?: IGame
}

export default IGameProps;