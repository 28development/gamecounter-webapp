const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD,
} = require('next/constants')

module.exports = {
  exportTrailingSlash: true,
  exportPathMap: function () {
    return {
      '/': { page: '/' }
    };
  }
};

// This uses phases as outlined here: https://nextjs.org/docs/#custom-configuration
module.exports = phase => {
  // when started in development mode `next dev` or `npm run dev` regardless of the value of STAGING environmental variable
  const isDev = phase === PHASE_DEVELOPMENT_SERVER
  // when `next build` or `npm run build` is used
  const isProd = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING !== '1'
  // when `next build` or `npm run build` is used
  const isStaging = phase === PHASE_PRODUCTION_BUILD && process.env.STAGING === '1'

  console.log(`isDev:${isDev}  isProd:${isProd}   isStaging:${isStaging}`)

  const env = {
    API_URL: (() => {
      if (isDev) return 'http://localhost:'
      if (isProd) return 'https://game-counter-io-api.now.sh'
      if (isStaging) return 'https://game-counter-io-api.now.sh'
      return 'http://localhost:'
    })(),
    API_PORT: (() => {
      if (isDev) return 4000
      if (isProd) return ""
      if (isStaging) return ""
      return 4000
    })(),
  }

  // next.config.js object
  return {
    env,
  }
}