import { useRouter } from 'next/router';
import Layout from '../../components/Layout';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import MainFeaturedGame from '../../components/MainFeaturedGame';
import IGame from '../../interfaces/game.interface';
import { useState, useEffect, Context, useLayoutEffect } from 'react';
import GamePost from '../../components/GamePost';
import { Box } from '@material-ui/core';
import nextCookie from 'next-cookies';
import { GetStaticProps, GetServerSideProps, NextPage, GetStaticPaths } from 'next';
import axios, { AxiosResponse } from 'axios';
import { motion } from 'framer-motion';
import GameProps from '../../interfaces/game.props.interface';
import Vibrant from 'node-vibrant';
import TopAppBar from '../../components/TopAppBar';
import { getGameRoute, getGamesRoute, setProxyImgUrl } from '../../lib/api/routes';
import Fade from '@material-ui/core/Fade';

const useStyles = makeStyles((theme) => ({
  mainGrid: {
    marginTop: theme.spacing(3),
  },
  cardGrid: {
    paddingBottom: theme.spacing(8),
  },
}));

let easing = [0.175, 0.85, 0.42, 0.96];

const textVariants = {
  exit: { y: 100, opacity: 0, transition: { duration: 0.5, ease: easing } },
  enter: {
    y: 0,
    opacity: 1,
    transition: { delay: 0.1, duration: 0.5, ease: easing }
  }
};

const imageVariants = {
  exit: {
    x: 100,
    opacity: 0,
    transition: {
      duration: 0.5,
      ease: easing
    }
  },
  enter: {
    x: 0,
    opacity: 1,
    transition: {
      delay: 0.5,
      duration: 0.5,
      ease: easing
    }
  }
};

const Game: NextPage<GameProps> = props => {
  const router = useRouter();
  const classes = useStyles();
  const [colorGradient, setColorGradient] = useState('linear-gradient(45deg, #1D70B6 50%, #29235C 90%)');

  useEffect(() => {
    window.scrollTo(0, 0);
    const proxyImgUrl = setProxyImgUrl(props?.game?.image);
    Vibrant.from(proxyImgUrl).getPalette()
      .then(palette => {
        if (palette.DarkMuted != undefined && palette.DarkMuted != null) {
          setColorGradient(`linear-gradient(45deg, ${palette.DarkMuted?.hex} 50%, ${palette.DarkVibrant?.hex} 90%)`);
        }
      })
      .catch(error => {
        console.log("Unable to get color from image");
        console.log(error)
      });
  });

  return (
    <Layout>
      <TopAppBar color={colorGradient} />
      <Container maxWidth="xl">
        <motion.div initial="exit" animate="enter" exit="exit">
          <motion.div variants={imageVariants}>
            <MainFeaturedGame post={props.game} />
          </motion.div>
          <motion.div variants={textVariants}>
            <Grid container spacing={4}>
              <GamePost key={props.game?.title} post={props.game} />
            </Grid>
          </motion.div>
          <Grid container spacing={5} className={classes.mainGrid}>
            {/* <Main title="From the firehose" posts={posts} />
            <Sidebar
              title={sidebar.title}
              description={sidebar.description}
              archives={sidebar.archives}
              social={sidebar.social}
            /> */}
          </Grid>
        </motion.div>
      </Container>
    </Layout>
  );
}

export const getStaticPaths: GetStaticPaths = async () => {
  const games = await axios.get(getGamesRoute);

  // Get the paths we want to pre-render based on posts
  const paths = games.data.map((game: IGame) => ({
    params: {
      title: game._id
    },
  }));

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: false }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const game = await axios.get(getGameRoute(params?.title));

  return {
    props: {
      game: game.data,
    },
  }
}

export default Game;
