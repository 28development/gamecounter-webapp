import { Container, Snackbar } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import { motion } from 'framer-motion';
import { GetStaticProps, NextPage } from 'next';
import React, { useState } from 'react';
import BottomNav from '../components/BottomNav';
import GameCard from '../components/GameCard';
import Layout from '../components/Layout';
import TopAppBar from '../components/TopAppBar';
import IGame from '../interfaces/game.interface';
import GameProps from '../interfaces/game.props.interface';
import { getGamesRoute } from '../lib/api/routes';
import GameContext from '../contexts/GameContext';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  }
}));

const gameVariants = {
  initial: { scale: 0.96, y: 30, opacity: 0 },
  enter: { scale: 1, y: 0, opacity: 1, transition: { duration: 0.5, ease: [0.48, 0.15, 0.25, 0.96] } },
  exit: {
    scale: 0.6,
    y: 100,
    opacity: 0,
    transition: { duration: 0.2, ease: [0.48, 0.15, 0.25, 0.96] }
  }
};

const Games: NextPage<GameProps> = props => {
  const classes = useStyles();
  const [colorGradient, setColorGradient] = useState('linear-gradient(45deg, #1D70B6 50%, #29235C 90%)');
  const [favMsg, setFavMsg] = useState('');
  const [open, setOpen] = useState(false);

  const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === 'clickaway') return;
    setOpen(false);
  };

  return (
    <Layout>
      <TopAppBar color={colorGradient} />
      <Container className={classes.cardGrid} maxWidth="md">
        <motion.div
          initial="initial"
          animate="enter"
          exit="exit"
          variants={{ exit: { transition: { staggerChildren: 0.1 } } }}
        >
          <Grid container spacing={1}>
            {props.games?.map((game: IGame, i: number) => (
              <Grid item key={game._id} xs={12} sm={6} md={3}>
                <motion.div variants={gameVariants}>
                  <motion.div
                    whileHover={{ scale: 1.05 }}
                    whileTap={{ scale: 0.95 }}
                  >
                    <GameContext.Provider value={game}>
                      <GameCard game={game}></GameCard>
                    </GameContext.Provider>
                  </motion.div>
                </motion.div>
              </Grid>
            ))
            }
          </Grid>
        </motion.div>
      </Container>
      <BottomNav></BottomNav>
    </Layout>
  );
}

export const getStaticProps: GetStaticProps = async context => {
  const { data } = await axios.get(getGamesRoute);

  return {
    props: {
      games: data
    },
  }
}

export default Games;