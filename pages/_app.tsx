import React from 'react';
import App from 'next/app';
import Head from 'next/head';
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { lightTheme, darkTheme } from '../src/theme';
import Router from 'next/router';
import NProgress from 'nprogress';
import 'react-dropzone-uploader/dist/styles.css';
import { trackPageView } from '../helpers';
import UserContext from '../contexts/UserContext';
import SnackbarContext from '../contexts/SnackbarContext';
import { Snackbar } from '@material-ui/core';
import { SharedBarProvider } from '../components/SharedSnackbarContext';

Router.events.on('routeChangeStart', url => {
  NProgress.start();
})
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

let theme = lightTheme;

export default class MyApp extends App {
  setTheme() {
    if (localStorage.getItem('darkMode') == 'dark') {
      console.log("is dark");
      theme = darkTheme;
    } else {
      console.log("is light");
      theme = lightTheme
    }
    console.log(theme);
  }

  componentDidMount() {
    this.setTheme();

    Router.events.on('routeChangeComplete', url => trackPageView(url));
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement!.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <React.Fragment>
        <Head>
          <title>GameCounter.io</title>
          <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
          <link rel="stylesheet" type="text/css" href="/nprogress.css" />
        </Head>
        {/* @todo https://reacttricks.com/sharing-global-data-in-next-with-custom-app-and-usecontext-hook/ */}
        {/* <UserContext.Provider value={{ user: this.state.user, signIn: this.signIn, signOut: this.signOut }}> */}
        <ThemeProvider theme={theme}>
          <SharedBarProvider>
            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            <Component {...pageProps} />
          </SharedBarProvider>
        </ThemeProvider>
        {/* </UserContext.Provider> */}
      </React.Fragment>
    );
  }
}