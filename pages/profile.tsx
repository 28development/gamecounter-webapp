import { useRouter } from 'next/router';
import Layout from '../components/Layout';
import NProgress from 'nprogress';
import { useEffect, useState } from 'react';
import TopAppBar from '../components/TopAppBar';

const Profile = () => {
  const [colorGradient, setColorGradient] = useState('linear-gradient(45deg, #1D70B6 50%, #29235C 90%)');

  return (
    <Layout>
      <TopAppBar color={colorGradient} />
      <h1>Use Profile Page</h1>
    </Layout>
  );
}

export default Profile;