export const trackPageView = (url: string) => {
  try {
    window.gtag('config', 'UA-164790691-1', {
      page_location: url
    });
  } catch (error) {
    // silences the error in dev mode
    // and/or if gtag fails to load
  }
}